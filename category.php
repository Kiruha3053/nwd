<?php get_header(); $used_posts = []; ?>
<?php
  $cat = get_the_category();

  $slug_array = ['Fashion', 'Wellness', 'Doitright', 'NewsEvents', 'Beauty'];
  
  if( count($cat) > 1 ){
    foreach( $cat as $cat_sep ){
      $str = str_replace(" ", "", str_replace("&amp;", "", $cat_sep->name));

      if( !in_array($str, $slug_array) ){
        $catID =  $cat_sep->cat_ID;
      }
    }
  } else {
    $catID = $cat[0]->cat_ID;
  }
?>
<main class='category'>
  <section class="category__title">
    <h1 class="<?php checkCat($cat);?>"><?= $cat[0]->cat_name  ?></h1>
  </section>
  
  <section class="category__content">
    
      <?php
        $cicle_counter = 0;
        
        $paged = get_query_var('paged') ? get_query_var('paged') : 0;
        $args = [
          'cat'           => $catID,
          'post_type'     => 'post',
          'posts_per_archive_page'=> 22,
          'paged' => $paged,
          'offset' => $paged,
          'post_status' => 'publish'
        ];
        $cat_posts = new WP_Query( $args );

        if($cat_posts->have_posts()){
          while($cat_posts->have_posts()){
            $cat_posts->the_post();
            // global $post;

            $cicle_counter++;
              switch ( $cicle_counter ){
                case(1):
                  include 'components/card_broken/card_broken.php';
                  break;              
                case(2):
                  include 'components/card_simple/card_simple.php';
                  break;              
                case(3):
                  include 'components/card_simple/card_simple.php';
                  break;              
                case(4):
                  include 'components/card_large/card_large.php';
                  break;              
                case(5):
                  include 'components/card_broken/card_broken.php';
                  break;              
                case(6):
                  include 'components/card_mini/card_mini.php';
                  break;              
                case(7):
                  include 'components/card_mini/card_mini.php';
                  break;              
                case(8):
                  include 'components/card_mini/card_mini.php';
                  break;              
                case(9):
                  include 'components/card_mini/card_mini.php';
                  break;              
                case(10):
                  include 'components/card_simple/card_simple.php';
                  break;              
                case(11):
                  include 'components/card_simple/card_simple.php';
                  break;              
                case(12):
                  include 'components/card_simple/card_simple.php';
                  break;              
                case(13):
                  include 'components/card_broken/card_broken.php';
                  break;              
                case(14):
                  include 'components/card_simple/card_simple.php';
                  break;              
                case(15):
                  include 'components/card_simple/card_simple.php';
                  break;
                case(16):
                  include 'components/card_mini/card_mini.php';
                  break;    
                case(17):
                  include 'components/card_mini/card_mini.php';
                  break;              
                case(18):
                  include 'components/card_mini/card_mini.php';
                  break;              
                case(19):
                  include 'components/card_mini/card_mini.php';
                  break;              
                case(20):
                  include 'components/card_broken/card_broken.php';
                  break;              
                case(21):
                  include 'components/card_simple/card_simple.php';
                  break;
                case(22):
                  include 'components/card_simple/card_simple.php';
                  break;
            }
          }  ?>
        <?php  }  ?>
  </section> 
  <section class="search__pagination">
    <?php
      $args = array(
      'show_all'     => false, // показаны все страницы участвующие в пагинации
      'end_size'     => 1,     // количество страниц на концах
      'mid_size'     => 1,     // количество страниц вокруг текущей
      'prev_next'    => true,  // выводить ли боковые ссылки "предыдущая/следующая страница".
      'prev_text'    => __('<svg width="60" height="8" viewBox="0 0 60 8" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M0.64645 3.64644C0.451187 3.8417 0.451187 4.15828 0.64645 4.35354L3.82843 7.53552C4.02369 7.73079 4.34027 7.73079 4.53554 7.53552C4.7308 7.34026 4.7308 7.02368 4.53554 6.82842L1.70711 3.99999L4.53554 1.17156C4.7308 0.976301 4.7308 0.659719 4.53554 0.464456C4.34028 0.269194 4.02369 0.269194 3.82843 0.464456L0.64645 3.64644ZM60 3.5L1 3.49999L1 4.49999L60 4.5L60 3.5Z" fill="#333333"/>
      </svg>
      '),
      'next_text'    => __('<svg width="60" height="8" viewBox="0 0 60 8" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M59.3535 4.35356C59.5488 4.1583 59.5488 3.84171 59.3535 3.64645L56.1716 0.464471C55.9763 0.269209 55.6597 0.269209 55.4645 0.464471C55.2692 0.659733 55.2692 0.976316 55.4645 1.17158L58.2929 4.00001L55.4645 6.82843C55.2692 7.02369 55.2692 7.34028 55.4645 7.53554C55.6597 7.7308 55.9763 7.7308 56.1716 7.53554L59.3535 4.35356ZM-4.37114e-08 4.5L59 4.50001L59 3.50001L4.37114e-08 3.5L-4.37114e-08 4.5Z" fill="#333333"/>
      </svg>
      '),
      'add_args'     => false, // Массив аргументов (переменных запроса), которые нужно добавить к ссылкам.
      'add_fragment' => '',     // Текст который добавиться ко всем ссылкам.
      );
      the_posts_pagination( $args );
    ?> 
  </section> 
</main>
<?php get_footer(); ?>