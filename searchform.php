<form class="search-form" role="search" method="get" id="searchform" action="<?php echo home_url( '/' ) ?>" >
  <button class="search-form__close" id="searchClose">
    <svg width="17" height="18" viewBox="0 0 17 18" fill="none" xmlns="http://www.w3.org/2000/svg">
      <rect x="1" y="1.15088" width="21.5415" height="0.979156" rx="0.489578" transform="rotate(45 1 1.15088)" fill="#333333"/>
    <rect y="16.1509" width="21.5415" height="0.97916" rx="0.48958" transform="rotate(-45 0 16.1509)" fill="#333333"/>
    </svg>
  </button>
  <input type="text" autocomplete="off" id="search-form__input"  class="search-form__input" placeholder="Search" value="<?php get_search_query(); ?>" name="s"  title="Search" max="100" maxLength="100" required/>
  <button class="search-form__submit-button">
    <svg width="37" height="37" viewBox="0 0 37 37" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M0.999998 36L11.7271 25.2729M11.7271 25.2729C14.3002 27.846 17.8548 29.4375 21.7812 29.4375C29.634 29.4375 36 23.0715 36 15.2188C36 7.36595 29.634 1 21.7812 0.999999C13.9285 0.999999 7.5625 7.36595 7.5625 15.2187C7.5625 19.1451 9.15399 22.6998 11.7271 25.2729Z" stroke="black" stroke-linecap="round" stroke-linejoin="round"/>
    </svg>
  </button>
</form>




