<?php get_header(); $used_posts = []; ?>
  <main class="single-main">
    <?php 
      setPostViews(get_the_ID());

      $cat = get_the_category();
      array_push($used_posts , get_the_ID());
    ?>
    <section class="sinle-content">
      <article>
        <a href="<?= get_category_link($cat[0]->term_id) ?>" class="single__cat <?php checkCat($cat); ?>"><?php echo $cat[0]->cat_name;  ?></a>
        <h1><?php the_title(); ?></h1>
        <div class="post_meta">
          <p class="meta__data"><?= get_the_date(); ?></p>
          <p class="meta__slash"> / </p>
          <div class="meta__views">
            Views <span><?php echo getPostViews(get_the_ID()); ?></span>
          </div>
          <p class="meta__slash"> / </p>
          <p class="meta__time-read">
          Time
          <span><?= round(strlen( wp_strip_all_tags(get_the_content())) / 1500); ?> min</span>
        </p>
        </div>
        <div class="single__article">
          <?php include 'components/lamoda__spinner.php' ?>
          <?php the_content() ?>
        </div>
        <div class="single__share">
          <ul>
            <li>
              <a href="https://www.instagram.com/lamodanistablog/" rel="nofollow" target="_blank">
                <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path fill-rule="evenodd" clip-rule="evenodd" d="M11.6628 0H4.32877C1.94185 0 0 1.94287 0 4.33105V11.6691C0 14.0571 1.94185 16 4.32877 16H11.6629C14.0497 16 15.9916 14.0571 15.9916 11.6691V4.33105C15.9916 1.94287 14.0497 0 11.6628 0ZM7.99579 12.3749C5.5847 12.3749 3.62321 10.4124 3.62321 8C3.62321 5.58765 5.5847 3.62512 7.99579 3.62512C10.4069 3.62512 12.3684 5.58765 12.3684 8C12.3684 10.4124 10.4069 12.3749 7.99579 12.3749ZM12.4729 4.65686C11.7604 4.65686 11.1809 4.07703 11.1809 3.36414C11.1809 2.65125 11.7604 2.07129 12.4729 2.07129C13.1854 2.07129 13.7651 2.65125 13.7651 3.36414C13.7651 4.07703 13.1854 4.65686 12.4729 4.65686ZM7.99451 5C6.34125 5 4.99609 6.34576 4.99609 8C4.99609 9.65413 6.34125 11 7.99451 11C9.64787 11 10.9929 9.65413 10.9929 8C10.9929 6.34576 9.64787 5 7.99451 5ZM12.1172 3.3645C12.1172 3.16895 12.2763 3.00977 12.4718 3.00977C12.6674 3.00977 12.8265 3.16882 12.8265 3.3645C12.8265 3.56018 12.6674 3.71924 12.4718 3.71924C12.2763 3.71924 12.1172 3.56006 12.1172 3.3645Z" fill="#333333"/>
                </svg>
              </a>
            </li>
            <li>
              <a href="https://twitter.com/intent/tweet?original_referer=http%3A%2F%2Ffiddle.jshell.net%2F_display%2F&text=<?php get_the_title(); ?>&url=<?php the_permalink(); ?>" rel="nofollow" target="_blank">
                <svg width="19" height="15" viewBox="0 0 19 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M18.4519 1.77577C17.7657 2.07692 17.0346 2.27654 16.2723 2.37346C17.0565 1.905 17.655 1.16885 17.9364 0.281538C17.2052 0.717692 16.398 1.02577 15.5377 1.19769C14.8434 0.458077 13.8539 0 12.7745 0C10.6802 0 8.99415 1.70077 8.99415 3.78577C8.99415 4.08577 9.01952 4.37423 9.0818 4.64885C5.9369 4.49538 3.15412 2.98731 1.28471 0.69C0.958346 1.25654 0.766907 1.905 0.766907 2.60308C0.766907 3.91385 1.44156 5.07577 2.44718 5.74846C1.83942 5.73692 1.2432 5.56038 0.738076 5.28231C0.738076 5.29385 0.738076 5.30885 0.738076 5.32385C0.738076 7.16308 2.04931 8.69077 3.7688 9.04269C3.46089 9.12692 3.12529 9.16731 2.77701 9.16731C2.53483 9.16731 2.29034 9.15346 2.06085 9.10269C2.55098 10.6015 3.94179 11.7035 5.59554 11.7392C4.30852 12.7465 2.67437 13.3535 0.905297 13.3535C0.595074 13.3535 0.297537 13.3396 0 13.3015C1.67566 14.3827 3.66155 15 5.80312 15C12.7641 15 16.5698 9.23077 16.5698 4.23C16.5698 4.06269 16.564 3.90115 16.556 3.74077C17.3067 3.20769 17.9376 2.54192 18.4519 1.77577Z" fill="#333333"/>
                </svg>
              </a>
            </li>
            <li>
              <a href="http://pinterest.com/pin/create/button/?url=<?php echo urlencode(get_permalink($post->ID)); ?>&media=<?php echo $pinterestimage[0]; ?>&description=<?php the_title(); ?>" rel="nofollow" target="_blank">
                <svg width="14" height="17" viewBox="0 0 14 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M11.6225 1.74206C10.453 0.618702 8.8344 0 7.06502 0C4.36221 0 2.69986 1.1085 1.78127 2.03836C0.64918 3.18431 0 4.7059 0 6.21307C0 8.10542 0.791116 9.55787 2.11594 10.0982C2.20489 10.1347 2.29438 10.153 2.38212 10.153C2.6616 10.153 2.88306 9.97008 2.95978 9.67657C3.0045 9.50819 3.10813 9.0928 3.15317 8.91246C3.24963 8.55634 3.1717 8.38505 2.96137 8.13705C2.5782 7.68345 2.39976 7.14704 2.39976 6.44895C2.39976 4.37538 3.94298 2.1716 6.80318 2.1716C9.07262 2.1716 10.4824 3.46213 10.4824 5.53954C10.4824 6.85047 10.2002 8.06451 9.68755 8.95819C9.33134 9.57914 8.70496 10.3193 7.74336 10.3193C7.32752 10.3193 6.95399 10.1484 6.71829 9.85049C6.49563 9.56883 6.42225 9.20498 6.5118 8.82578C6.61296 8.39733 6.7509 7.95041 6.8844 7.51835C7.12788 6.72924 7.35805 5.98393 7.35805 5.3893C7.35805 4.3722 6.73309 3.6888 5.80305 3.6888C4.62109 3.6888 3.69511 4.88991 3.69511 6.42323C3.69511 7.17523 3.89486 7.73767 3.98528 7.95365C3.83639 8.58485 2.95145 12.3375 2.78359 13.0451C2.68654 13.4582 2.10186 16.7209 3.0696 16.981C4.15692 17.2733 5.12883 14.0957 5.22774 13.7366C5.30792 13.4446 5.58845 12.3404 5.76036 11.6616C6.28525 12.1674 7.1304 12.5094 7.95275 12.5094C9.50304 12.5094 10.8972 11.8114 11.8786 10.5441C12.8303 9.31497 13.3545 7.60175 13.3545 5.72031C13.3545 4.24944 12.7232 2.7994 11.6225 1.74206Z" fill="#333333"/>
                </svg>
              </a>
            </li>
            <li>
              <a href="http://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>" rel="nofollow" target="_blank">
                <svg width="10" height="17" viewBox="0 0 10 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M8.84023 0.00353713L6.63689 0C4.16151 0 2.56182 1.64211 2.56182 4.18372V6.1127H0.346454C0.155021 6.1127 0 6.26798 0 6.45951V9.25438C0 9.44592 0.155197 9.60102 0.346454 9.60102H2.56182V16.6534C2.56182 16.8449 2.71684 17 2.90827 17H5.79869C5.99012 17 6.14514 16.8447 6.14514 16.6534V9.60102H8.73541C8.92685 9.60102 9.08187 9.44592 9.08187 9.25438L9.08293 6.45951C9.08293 6.36755 9.04634 6.27948 8.98147 6.21439C8.9166 6.14931 8.82821 6.1127 8.7363 6.1127H6.14514V4.47748C6.14514 3.69153 6.33233 3.29254 7.35561 3.29254L8.83988 3.29201C9.03114 3.29201 9.18616 3.13673 9.18616 2.94537V0.350176C9.18616 0.158994 9.03131 0.00389085 8.84023 0.00353713Z" fill="#333333"/>
                </svg>
              </a>
            </li>
            <li>
              <a href="https://www.youtube.com/channel/UCYaWj5Hays081vQkgl_wrFQ" rel="nofollow" title="YotTube_Share" target="_blank">
                <svg width="20" height="14" viewBox="0 0 20 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M19.5733 2.19067C19.343 1.33405 18.668 0.658786 17.812 0.428126C16.2481 0 9.99233 0 9.99233 0C9.99233 0 3.73685 0 2.17295 0.411894C1.33336 0.64231 0.641857 1.33417 0.411559 2.19067C0 3.75525 0 7 0 7C0 7 0 10.2611 0.411559 11.8093C0.642101 12.6658 1.31689 13.3411 2.17307 13.5718C3.75331 14 9.99257 14 9.99257 14C9.99257 14 16.2481 14 17.812 13.5881C18.6681 13.3576 19.343 12.6823 19.5736 11.8258C19.985 10.2611 19.985 7.01648 19.985 7.01648C19.985 7.01648 20.0015 3.75525 19.5733 2.19067ZM8.00064 9.99761V4.00239L13.2026 7L8.00064 9.99761Z" fill="#333333"/>
                </svg>
              </a>
            </li>
          </ul>
          <?php
      // show_lists=1 - after like/dislike show list with reasons to like/dislike this article
      // show_lists=0 - after like/dislike not show list with reasons to like/dislike this article
      echo do_shortcode("[rates_block show_lists=0 ]"); ?>
        </div>
        <h2>Leave Comment</h2>
        <?php comments_template(); ?>
      </article>
      <aside>
        <div class="aside__posts">
          <?php
            $args = [
              'post__not_in' => $used_posts,
              'category_name' => $cat[0]->cat_name,
              'posts_per_page' => 2,
            ];
            $aside_posts = new WP_Query($args);

            if($aside_posts->have_posts()){
              while($aside_posts->have_posts()){
                $aside_posts->the_post();
                
                include 'components/card_simple/card_simple.php';
              }
              wp_reset_postdata();
            }
          ?>
        </div>
        <div class="aside__ads">
          <div class="test__ad"></div>
        </div>
      </aside>
    </section>
    <section class="single__comments">
      
    </section>
    <?php include 'components/top_news/top_news.php' ?>
  </main>
<?php get_footer(); ?>