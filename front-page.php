<?php
    /**
     * Template Name: Front-page
     * 
     * 
     */
?>
<?php get_header(); $used_posts = []; ?>
  <main>
    <section class="front-section">
      <div class="container">
        <?php
          $args = [
            'posts_per_page' => 1,
            'post_type'   => 'post',
            'suppress_filters' => true, 
            'orderby' => 'date'
          ];
          $front_intro = new WP_Query($args);
          
          if($front_intro->have_posts()){
            while($front_intro->have_posts()){
              $front_intro->the_post();
              array_push($used_posts, get_the_ID()); ?>
              
              <div class="front-intro">
                <div class="front-intro__block">
                  <div class="intro-block__content">
                    <span><?php $category = get_the_category(); echo $category[0]->name; ?></span>
                    <h4><?= get_the_title(); ?></h4>

                  </div>
                  <button class="intro-block_read">
                    <span>Read article</span>
                    <svg width="26" height="8" viewBox="0 0 26 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path d="M25.3536 4.35355C25.5488 4.15829 25.5488 3.84171 25.3536 3.64645L22.1716 0.464466C21.9763 0.269204 21.6597 0.269204 21.4645 0.464466C21.2692 0.659728 21.2692 0.976311 21.4645 1.17157L24.2929 4L21.4645 6.82843C21.2692 7.02369 21.2692 7.34027 21.4645 7.53553C21.6597 7.7308 21.9763 7.7308 22.1716 7.53553L25.3536 4.35355ZM0 4.5L25 4.5V3.5L0 3.5L0 4.5Z" fill="#F2994A"/>
                    </svg>
                  </button>
                </div>
                <div class="front-intro__media">
                  <img src = "<?= the_post_thumbnail_url() ?>" 
                       alt = "<?= get_post_meta(get_post_thumbnail_id(), '_wp_attachment_image_alt', true) ?>"
                       class = ""
                  />
                </div>
              </div>
            <?php }
            wp_reset_postdata();
          }
        ?>
      </div>
    </section>
    <section class="front-section">
      <div class="container">
        <div class="front-section__heading">
          <h2>Air</h2>
        </div>
      </div>
    </section>
    <section class="front-section">
      <div class="container">
        <div class="front-section__heading">
          <h2>Earth</h2>
        </div>
      </div>
    </section>
    <section class="front-section">
      <div class="container">
        <div class="front-section__heading">
          <h2>Fire</h2>
        </div>
      </div>
    </section>
    <section class="front-section">
      <div class="container">
        <div class="front-section__heading">
          <h2>Water</h2>
        </div>
      </div>
    </section>
    <section class="front-section">
      <div class="container">
        <div class="front-section__heading">
          <h2>What to do</h2>
        </div>
      </div>
    </section>
    <section class="front-section">
      <div class="container">
        <div class="front-section__heading">
          <h2>Man-Made</h2>
        </div>
      </div>
    </section>
    <section class="front-section">
      <div class="container">
        <div class="front-section__heading">
          <h2>Interesting</h2>
        </div>
      </div>
    </section>
  </main>
<?php get_footer() ?>