<?php 
add_action( 'wp_enqueue_scripts', 'style_theme' );
add_action( 'wp_enqueue_scripts', 'scripts_theme' );
add_theme_support( 'html5', array( 'search-form' ) );
add_theme_support( 'post-thumbnails', array( 'post' ) );
add_theme_support( 'post-formats', array( 'aside', 'gallery' ) );
add_theme_support( 'post-thumbnails' );


function style_theme() {
    wp_dequeue_style( 'original-enqueue-stylesheet-handle' );
    wp_deregister_style( 'original-register-stylesheet-handle' );

    if(is_front_page()){
      wp_dequeue_style( 'wp-block-library' );
      wp_dequeue_style( 'wp-block-library-theme' );
      wp_dequeue_style( 'font-awesome-css' );
    }
    wp_register_style( 'parent-style', get_template_directory_uri() . '/style.css', null, time() );
    wp_enqueue_style( 'parent-style' );

    // if(is_front_page()){
    //   wp_enqueue_style( 'style-addon', get_template_directory_uri() . '/assets/css/animate.min.css', array(), time(), true );
    // }

    
    // $user_agent = $_SERVER['HTTP_USER_AGENT'];
    // if( strrpos($user_agent , "Chrome") !== false ){
    //     wp_enqueue_style('style-addon', get_template_directory_uri() . '/safari.css', array(), time(), true );
    // }
}
function scripts_theme(){
    
    if(is_front_page()){
      wp_deregister_script( 'jquery' );
    }
    
    wp_register_script( 'pwa', 'https://lamodanista.com/sw.js', null, time() );
    wp_register_script( 'index-js', get_template_directory_uri() . '/index-min.js', null, time() );
    wp_enqueue_script( 'index-js');
    wp_enqueue_script( 'pwa');
    
}
add_action('after_setup_theme', 'theme_register_nav_menu');

function theme_register_nav_menu(){
	register_nav_menu('footer_contacts','footer_contacts');
	register_nav_menu('footer_cats','footer_cats');
  register_nav_menu('header','header');
  register_nav_menu('burger','burger');
	add_theme_support('post-thumbnails', array('post'));
	add_theme_support('title-tag');
	add_filter( 'excerpt_length', function(){
		return 25;
	} );
	add_filter('excerpt_more', function($more) {
		return '...';
	});
}
function my_navigation_template( $template, $class ){
    return '
    <nav class="navigation %1$s" role="navigation">
        <div class="nav-links">%3$s</div>
    </nav>    
    ';
}
add_filter('navigation_markup_template', 'my_navigation_template', 10, 2 );









add_action( 'wp_enqueue_scripts', 'enqueue_font_awesome' );
function enqueue_font_awesome() {
	wp_enqueue_style( 'font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css' );
}





// function the_breadcrumb() {
//     echo '<div id="breadcrumb"><ul><li><a href="/">Home</a></li><li>></li>';

//     if ( is_category() || is_single() ) {
//         $cats = get_the_category();
//         $cat = $cats[0];
//         echo '<li><a href="'.get_category_link($cat->term_id).'"|'.$cat->name.'</a></li><li>></li>';
//     }

//     if(is_single()){
//         echo '<li>';
//         the_title();
//         echo '</li>';
//     }

//     if(is_page()){
//         echo '<li>';
//         the_title();
//         echo '</li>';
//     }

//     echo '</ul><div class="clear"></div></div>';
// }

function kama_excerpt( $args = '' ){
    global $post;

    if( is_string($args) )
        parse_str( $args, $args );

    $rg = (object) array_merge( array(
        'maxchar'     => 350,
        'text'        => '',    
        'autop'       => true,  
        'save_tags'   => '',    
        'ignore_more' => false,
    ), $args );

    $rg = apply_filters( 'kama_excerpt_args', $rg );

    if( ! $rg->text )
        $rg->text = $post->post_excerpt ?: $post->post_content;

    $text = $rg->text;
    $text = preg_replace( '~\[([a-z0-9_-]+)[^\]]*\](?!\().*?\[/\1\]~is', '', $text );
    $text = preg_replace( '~\[/?[^\]]*\](?!\()~', '', $text );
    $text = trim( $text );
    if( ! $rg->ignore_more  &&  strpos( $text, '<!--more-->') ){
        preg_match('/(.*)<!--more-->/s', $text, $mm );

        $text = trim( $mm[1] );

        $text_append = ' <a href="'. get_permalink( $post ) .'#more-'. $post->ID .'">'. $rg->more_text .'</a>';
    }
    else {
        $text = trim( strip_tags($text, $rg->save_tags) );
        if( mb_strlen($text) > $rg->maxchar ){
            $text = mb_substr( $text, 0, $rg->maxchar );
            $text = preg_replace( '~(.*)\s[^\s]*$~s', '\\1...', $text );
        }
    }
    if( $rg->autop ){
        $text = preg_replace(
            array("/\r/", "/\n{2,}/", "/\n/",   '~</p><br ?/?>~'),
            array('',     '</p><p>',  '<br />', '</p>'),
            $text
        );
    }

    $text = apply_filters( 'kama_excerpt', $text, $rg );

    if( isset($text_append) )
        $text .= $text_append;

    return ( $rg->autop && $text ) ? "<p>$text</p>" : $text;
}


add_filter( 'wpseo_next_rel_link', 'custom_change_wpseo_next_home_remove' );
function custom_change_wpseo_next_home_remove( $link ) {
    if ( is_front_page() ) {
        $link = '';
    }
    return $link;
}

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page();
	
}

function getPostViews($postID){
  $count_key = 'post_views_count';
  $count = get_post_meta($postID, $count_key, true);
  if($count==''){
      delete_post_meta($postID, $count_key);
      add_post_meta($postID, $count_key, '0');
      return "0";
  }
  return $count;
}
function setPostViews($postID) {
  $count_key = 'post_views_count';
  $count = get_post_meta($postID, $count_key, true);
  if($count==''){
      $count = 0;
      delete_post_meta($postID, $count_key);
      add_post_meta($postID, $count_key, '0');
  }else{
      $count++;
      update_post_meta($postID, $count_key, $count);
  }
}
function posts_column_views($defaults) {
  $defaults["post_views_count"] = __("Views");
  return $defaults;
}


function wph_exclude_pages($query) {
  if ($query->is_search) {
      $query->set('post_type', 'post');
  }
  return $query;
}
add_filter('pre_get_posts','wph_exclude_pages');
add_filter( 'kdmfi_featured_images', function( $featured_images ) {
  // Add featured-image-2 to pages and posts
  $args_1 = array(
    'id' => 'featured-image-2',
    'desc' => 'Your description here.',
    'label_name' => 'Featured Image 2',
    'label_set' => 'Set featured image 2',
    'label_remove' => 'Remove featured image 2',
    'label_use' => 'Set featured image 2',
    'post_type' => array( 'page', 'post' ),
  );

  $featured_images[] = $args_1;

  // Important! Return all featured images
  return $featured_images;
});

add_filter( 'wpseo_opengraph_image ', 'op_img' );
function op_img($filter){
  if(is_category()){
    return false;
  }
  return $filter;
}

remove_action( 'wp_head', 'rsd_link' );
remove_action( 'wp_head', 'wp_generator' );
remove_action( 'wp_head', 'index_rel_link' );
remove_action( 'wp_head', 'wlwmanifest_link' );
remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0 );
remove_action( 'wp_head', 'wp_shortlink_wp_head', 10, 0 );
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );


/* Dima tut staralsa */
add_action('enqueue_block_editor_assets', 'block_editor_text_highlight_button');
function block_editor_text_highlight_button() {

// Load the compiled blocks into the editor.
wp_enqueue_script('text-highlight-button-js', get_template_directory_uri().'/assets/JS/text-highlight-button.js', array( 'wp-blocks', 'wp-element', 'wp-components', 'wp-editor' ), '2.0', true);

// Load the compiled styles into the editor.
wp_enqueue_style('text-highlight-button-editor-css', get_template_directory_uri().'/assets/css/text-highlight-button.css?ver=222', array( 'wp-edit-blocks' ));
}



add_action( 'admin_head', 'my_stylesheet' );
function my_stylesheet(){
  echo '<link href="'.get_bloginfo( 'stylesheet_directory' ).'/assets/css/text-highlight-button.css?ver='.  time() .' rel="stylesheet" type="text/css">';
 }

/* Dima zakonchil staratsa */

/**
 * Disable the emoji's
 */
function disable_emojis() {
  remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
  remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
  remove_action( 'wp_print_styles', 'print_emoji_styles' );
  remove_action( 'admin_print_styles', 'print_emoji_styles' ); 
  // remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
  remove_filter( 'comment_text_rss', 'wp_staticize_emoji' ); 
  remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
  add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
  add_filter( 'wp_resource_hints', 'disable_emojis_remove_dns_prefetch', 10, 2 );
 }
 add_action( 'init', 'disable_emojis' );
 
 /**
  * Filter function used to remove the tinymce emoji plugin.
  * 
  * @param array $plugins 
  * @return array Difference betwen the two arrays
  */
 function disable_emojis_tinymce( $plugins ) {
  if ( is_array( $plugins ) ) {
  return array_diff( $plugins, array( 'wpemoji' ) );
  } else {
  return array();
  }
 }
 
 /**
  * Remove emoji CDN hostname from DNS prefetching hints.
  *
  * @param array $urls URLs to print for resource hints.
  * @param string $relation_type The relation type the URLs are printed for.
  * @return array Difference betwen the two arrays.
  */
 function disable_emojis_remove_dns_prefetch( $urls, $relation_type ) {
  if ( 'dns-prefetch' == $relation_type ) {
  /** This filter is documented in wp-includes/formatting.php */
  $emoji_svg_url = apply_filters( 'emoji_svg_url', 'https://s.w.org/images/core/emoji/2/svg/' );
 
 $urls = array_diff( $urls, array( $emoji_svg_url ) );
  }
 
 return $urls;
 }