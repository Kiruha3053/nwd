<?php
//Get only the approved comments
$args = array(
    'status' => 'approve',
    'post_id'=>get_the_ID()
);
// The comment Query
$comments_query = new WP_Comment_Query;
$comments = $comments_query->query( $args );
if ( $comments ) {
?>
<ul class="comments_list">
  <?

   foreach ( $comments as $comment ) {
     $link = get_comment_reply_link(array(
      	'reply_text' => "Answer",
      	'respond_id' => 'comment',
      	'depth' => 5,
      	'max_depth' => 10,
      ), $comment, get_the_ID() );
      ?>
      <li id="comment-<?=$comment->comment_ID?>">
        <div class="comment-header">
          <h4><?=$comment->comment_author?></h4>
          <p><?=$comment->comment_date?></p>
        </div>
        <p>
          <?php if ($comment->comment_parent != 0): ?>
            <?php $answer_to = get_comment($comment->comment_parent) ?>
            To <a href="#comment-<?=$comment->comment_parent?>"><?=$answer_to->comment_author?></a>,
          <?php endif; ?>
          <?=$comment->comment_content?>
        </p>
      </li>
      <?php
   }

  ?>
</ul>
<?php }
elseif (!$comments){
  echo "<div class ='message__no-comments'>";
  echo "<p>We don’t have comments on this article yet.  \n </p>"; 
  echo "<p>You can be first. For leaving a comment go to the section.  \n </p>";
  echo "</div>";
}
