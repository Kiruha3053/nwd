window.addEventListener('load', (windowLoadEvent)=> {
  windowLoadEvent.preventDefault()

  /* 
    Global Variables start
  */
  let header = <HTMLElement>document.querySelector('header')
  let scrollProgress = <HTMLElement>document.querySelector('.page__scroll-progress')
  let scrollState = 0
  /* 
    Global Variables end
  */

  
  window.addEventListener('scroll', (windowScrollEvent)=> {
    let windowScroll = document.body.scrollTop || document.documentElement.scrollTop;
    let windowHeight =  document.documentElement.scrollHeight - document.documentElement.clientHeight;
    let procent = windowScroll / windowHeight * 100

    scrollState++
    console.log(scrollState)
    console.log(document.documentElement.getBoundingClientRect())


    scrollProgressCalculate(scrollProgress, procent)
    headerOnScroll(windowScroll, windowHeight)
  })
})


function scrollProgressCalculate(element: HTMLElement, procent: number){
  element.style.width = procent + '%'
} 
function headerOnScroll(srcoll: number, height: number){
  // console.log(srcoll)
  // console.log(height)
}