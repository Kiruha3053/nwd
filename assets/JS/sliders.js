let topSlider = new Swiper('.home__slider-container', {
  slidesPerView: 1,
  speed: 200,
  allowTouchMove: true,
  loop: true,
  initialSlide: 0,
  effect: 'fade',
  shortSwipes: false,
  fadeEffect: {
    crossFade: true
  },
  navigation: false,
  pagination: true,
  autoplay: {
    delay: 5000
  },
  pagination: {
    el: '.home-slider__pagination-block',
    type: "fraction",
  },
  breakpoints: {
    1200: {
      allowTouchMove: false,
      navigation: {
        nextEl: '.home-slider__button-next',
        prevEl: '.home-slider__button-prev',
      },
    }
  }
});

// let topSliderPrev = new Swiper('.prev__slider-container', {
//   slidesPerView: 1,
//   speed: 200,
//   allowTouchMove: false,
//   loop: true,
//   initialSlide: 1,
//   effect: 'fade',
//   shortSwipes: false,
//   fadeEffect: {
//     crossFade: true
//   },
//   navigation: false,
//   autoplay: {
//     delay: 5000
//   },
//   // pagination: {
//   //   el: '.top-slider__pagination',
//   //   type: 'bullets',
//   // },
//   breakpoints: {
//     1200: {
//       pagination: false,
//       navigation: {
//         nextEl: '.home-slider__button-next',
//         prevEl: '.home-slider__button-prev',
//       },
//     }
//   }
// });