<?php
  /**
   * Template Name: Contact page
   */
  ?>
<?php get_header(); $slide = array(); ?>
  <main class="page__contact-us">
    <div class="contact-us__content">
      <h1><span><?php the_title() ?></span></h1>
      <?php the_content() ?>
      <div class="form-block">

        <?php echo do_shortcode('[contact-form-7 id="3595" title="Redisgn Contact form"]'); ?>
        
        <!-- <form class="contact__form" action="/contacts/#wpcf7-f3595-o1" method="post">
          <div class="contact__name">
            <label for="contact__name">Your Name</label>
            <input id="contact__name" class="untoched" type="text" name="contact__name" placeholder="Name">
            <label class="contact__name-error error__message"></label>
          </div>
          <div class="contact__email">
            <label for="contact__email">Your Email</label>
            <input id="contact__email" class="untoched" type="email" name="contact__email" placeholder="Email">
            <label class="contact__email-error error__message"></label>
          </div>
          <div class="contact__text-area">
            <label for="contact__text-area">Your Message</label>
            <textarea id="contact__text-area" class="untoched" name="contact__text-area" cols="30" rows="10" placeholder="Message"></textarea>
            <label class="text-area__error error__message"></label>
          </div>
          <button class="contact__submit">
            <span>Send</span>
            <svg width="28" height="8" viewBox="0 0 28 8" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M27.3536 4.35354C27.5488 4.15828 27.5488 3.8417 27.3536 3.64644L24.1716 0.464457C23.9763 0.269195 23.6597 0.269195 23.4645 0.464457C23.2692 0.659719 23.2692 0.976302 23.4645 1.17156L26.2929 3.99999L23.4645 6.82842C23.2692 7.02368 23.2692 7.34026 23.4645 7.53553C23.6597 7.73079 23.9763 7.73079 24.1716 7.53552L27.3536 4.35354ZM1.89416e-07 4.5L27 4.49999L27 3.49999L-1.89416e-07 3.5L1.89416e-07 4.5Z" fill="#333333"/>
            </svg>
          </button>
        </form> -->
      </div>
    </div>
    <?php // include 'components/lamoda__spinner.php' ?>
  </main>
  <div class="page-contact__pop-up">
    <div class="pop-up__block">
      <h3 class="pop-up__heading">Thank you!</h3>
      <p class="pop-up__text">We have received your message!</p>
      <button class="pop-up__close">
        <svg width="17" height="17" viewBox="0 0 17 17" fill="none" xmlns="http://www.w3.org/2000/svg">
          <rect x="1" y="1" width="21.5415" height="0.979156" rx="0.489578" transform="rotate(45 1 1)" fill="#333333"/>
          <rect y="16" width="21.5415" height="0.97916" rx="0.48958" transform="rotate(-45 0 16)" fill="#333333"/>
        </svg>
      </button>
    </div>
  </div>
<?php get_footer(); ?>

