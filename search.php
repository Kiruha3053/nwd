<?php get_header(); $slide = array(); ?>

    <main class="search-page">
        <?php if( have_posts() ){ ?>
          <section class="search_result">
            <?php global $wp_query;?>      
            <h1>Search for '<?php echo get_search_query() ?>'</h1>    
          </section>
          <section class="search__main">
            <div class="search-grid">
              <?php
              $the_query= $wp_query;
              while( $the_query->have_posts()){
                $the_query->the_post();?>
                <?php  include 'components/card_simple/card_simple.php'; ?>
              <?php } ?>
            </div>
          </section>
          <section class="search__pagination">
            <?php
              $args = array(
              'show_all'     => false, // показаны все страницы участвующие в пагинации
              'end_size'     => 1,     // количество страниц на концах
              'mid_size'     => 1,     // количество страниц вокруг текущей
              'prev_next'    => true,  // выводить ли боковые ссылки "предыдущая/следующая страница".
              'prev_text'    => __('<svg width="60" height="8" viewBox="0 0 60 8" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M0.64645 3.64644C0.451187 3.8417 0.451187 4.15828 0.64645 4.35354L3.82843 7.53552C4.02369 7.73079 4.34027 7.73079 4.53554 7.53552C4.7308 7.34026 4.7308 7.02368 4.53554 6.82842L1.70711 3.99999L4.53554 1.17156C4.7308 0.976301 4.7308 0.659719 4.53554 0.464456C4.34028 0.269194 4.02369 0.269194 3.82843 0.464456L0.64645 3.64644ZM60 3.5L1 3.49999L1 4.49999L60 4.5L60 3.5Z" fill="#333333"/>
              </svg>
              '),
              'next_text'    => __('<svg width="60" height="8" viewBox="0 0 60 8" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M59.3535 4.35356C59.5488 4.1583 59.5488 3.84171 59.3535 3.64645L56.1716 0.464471C55.9763 0.269209 55.6597 0.269209 55.4645 0.464471C55.2692 0.659733 55.2692 0.976316 55.4645 1.17158L58.2929 4.00001L55.4645 6.82843C55.2692 7.02369 55.2692 7.34028 55.4645 7.53554C55.6597 7.7308 55.9763 7.7308 56.1716 7.53554L59.3535 4.35356ZM-4.37114e-08 4.5L59 4.50001L59 3.50001L4.37114e-08 3.5L-4.37114e-08 4.5Z" fill="#333333"/>
              </svg>
              '),
              'add_args'     => false, // Массив аргументов (переменных запроса), которые нужно добавить к ссылкам.
              'add_fragment' => '',     // Текст который добавиться ко всем ссылкам.
              );
              the_posts_pagination( $args );
            ?> 
          </section>       
        <?php } else { ?>
        <div class="search_result">
          <?php global $wp_query;?>
          <h1>Search for '<?php echo get_search_query() ?>'</h1>
          <h2 class="sorry">Sorry, there were no results. Please try again, or see current topics today</h2>
        </div>
        <section class="search__main">
          <div class="search-grid">
              <?php  $i=0; $no = get_field('not_found',13);
              if($no==null){ 
              $no = get_posts( array(
                  'numberposts' => 12,
                  
                  'post_type'   => 'post',
                  'suppress_filters' => true, 
              ) );
              }?>

              <?php foreach( $no as $post ){
                setup_postdata($post); 
                if($i<12){ ?>
                <?php include 'components/card_simple/card_simple.php'; ?>
              <?php } else { break; } $i++;} wp_reset_postdata();?>
          </div>
        </section>
        
      <?php } ?>
    </main>

<?php get_footer(); ?>